package learn.edacy.fragments;

import android.content.Context;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;

public class InscriptionFragment extends Fragment {
    /**
     * Interface servant de callback
     * Pour envoyer les donnees du fragment a l'activite.
     */
    interface SendDataToActivityListener {
        /**
         *
         * @param firstname qui a ete saisi dans l'edittext du prenom
         * @param lastname qui a ete saisi dans l'edittext du nom
         */
        void send(String firstname, String lastname);
    }

    private SendDataToActivityListener mListener;
    private static final String TAG = "InscriptionFragment";
    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        try {
            //On initialise le listener qui va permettre d'envoyer les
            //donnees a l'activite
            mListener = (SendDataToActivityListener) context;
        } catch (ClassCastException ex) {
            Log.d(TAG, "onAttach: " + ex.getMessage());
        }
    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        super.onCreateView(inflater, container, savedInstanceState);
        return inflater.inflate(R.layout.add_talent, container, false);
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        final EditText prenomTv = view.findViewById(R.id.prenom);
        final EditText nomTv = view.findViewById(R.id.nom);
        //J'envoie les donnees saisies a mon activite qd je clique sur le bouton submit
        view.findViewById(R.id.submit).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String prenom = prenomTv.getText().toString();
                String nom = nomTv.getText().toString();
                if (prenom.equals(""))
                    prenomTv.setError("Saisir le prenom");
                else if(nom.equals(""))
                    nomTv.setError("Saisir le nom");
                else
                    mListener.send(prenom, nom);
            }
        });

    }
}

