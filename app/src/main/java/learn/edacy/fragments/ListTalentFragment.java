package learn.edacy.fragments;


import android.content.Context;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;
import android.widget.Toast;

public class ListTalentFragment extends Fragment {

    private static final String TAG = "ListTalentFragment";
    private Context mContext;
    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        mContext = context;
    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        super.onCreateView(inflater, container, savedInstanceState);
        Log.d(TAG, "onCreateView: ");
        return inflater.inflate(R.layout.list_talent, container, false);

    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        TextView prenomTv = view.findViewById(R.id.prenom);
        TextView nomTv = view.findViewById(R.id.nom);
        Bundle bundle = getArguments();
        if (bundle != null) {
            String prenom = bundle.getString("prenom");
            String nom = bundle.getString("nom");
            prenomTv.setText(prenom);
            nomTv.setText(nom);
        } else
            Toast.makeText(mContext, R.string.no_data_msg, Toast.LENGTH_SHORT).show();





    }
}
