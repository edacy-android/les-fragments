package learn.edacy.fragments;

import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.NavigationView;
import android.support.design.widget.Snackbar;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.support.v4.view.GravityCompat;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;

public class MainActivity extends AppCompatActivity
        implements NavigationView.OnNavigationItemSelectedListener, InscriptionFragment.SendDataToActivityListener {

    //L'objet qui s'occupe de la gestion de fragments
    private FragmentManager fragmentManager;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        Toolbar toolbar = findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        //recuperer mon fragment manager qui permet de gerer les fragments dans mon activites
        fragmentManager = getSupportFragmentManager();

        DrawerLayout drawer = findViewById(R.id.drawer_layout);
        ActionBarDrawerToggle toggle = new ActionBarDrawerToggle(
                this, drawer, toolbar, R.string.navigation_drawer_open, R.string.navigation_drawer_close);
        drawer.addDrawerListener(toggle);
        toggle.syncState();

        NavigationView navigationView = findViewById(R.id.nav_view);
        //Associe un clic aux elements du menu
        navigationView.setNavigationItemSelectedListener(this);


        //Par defaut placer le fragement InscriptionFragment
        InscriptionFragment inscriptionFragment = new InscriptionFragment();
        placeFragment(inscriptionFragment);

    }

    /**
     * Permet le fermer le menu s'il est ouvert
     * quand on clique sur retour
     */
    @Override
    public void onBackPressed() {
        DrawerLayout drawer = findViewById(R.id.drawer_layout);
        if (drawer.isDrawerOpen(GravityCompat.START)) {
            drawer.closeDrawer(GravityCompat.START);
        } else {
            super.onBackPressed();
        }
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }

    /**
     * Gerer les clics sur les items du menu
     * @param item selection
     * @return #true if an item is select
     */
    @SuppressWarnings("StatementWithEmptyBody")
    @Override
    public boolean onNavigationItemSelected(@NonNull MenuItem item) {
        int id = item.getItemId();

        if (id == R.id.ajout) {
            //Je cree une instance de mon fragment List
            InscriptionFragment talentFragment = new InscriptionFragment();
            //On place le fragment dans l'activite
            placeFragment(talentFragment);
            // Handle the ajout action
        } else if (id == R.id.list) {
            //Je cree une instance de mon fragment List
            ListTalentFragment listFragment = new ListTalentFragment();
            placeFragment(listFragment);
        }  else if (id == R.id.nav_share) {

        }

        DrawerLayout drawer = findViewById(R.id.drawer_layout);
        drawer.closeDrawer(GravityCompat.START);
        return true;
    }

    /**
     * Methode utilitaire qui placera le fragment dans l'activite
     * @param fragment qui va etre place dans l'activite
     */
    private void placeFragment(Fragment fragment) {

        FragmentTransaction fragmentTransaction = fragmentManager.beginTransaction();
        fragmentTransaction.replace(R.id.frame, fragment);
        fragmentTransaction.commit();
    }

    /**
     * Implementation de la methode de l'interface definie
     * dans le fragment InscriptionFragment
     * Elle recupere le nom et le prenom qu'elle envoie au framgent AjoutFragment
     * A l'aide d'un Bundle
     * @param firstname qui a ete saisi dans l'edittext du prenom
     * @param lastname qui a ete saisi dans l'edittext du nom
     */
    @Override
    public void send(String firstname, String lastname) {
        Bundle bundle = new Bundle();
        ListTalentFragment listFragment = new ListTalentFragment();
        bundle.putString("prenom", firstname);
        bundle.putString("nom", lastname);
        listFragment.setArguments(bundle);
        placeFragment(listFragment);
    }
}
